module.exports = {
  /**
  * 风险提醒
  * @param ctx 实例
  * @param err 错误信息 Object
  * @param errorMsg 错误信息
  */
  errorRequest(ctx, err, errorMsg) {
    console.error(errorMsg);
    ctx.app.emit('error', err, ctx);
  }
}