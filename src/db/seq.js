const { MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PWD, MYSQL_DB } = require('../config/config.default')
// 基于node.js的ORM框架，它可以让我们使用js操作数据库，不用关心底层的sql语句
const { Sequelize } = require('sequelize');

const seq = new Sequelize(MYSQL_DB, MYSQL_USER, MYSQL_PWD, {
  localhost: MYSQL_HOST + MYSQL_PORT,
  dialect: 'mysql',
  // logging: (...msg) => console.log(msg), // 显示所有日志函数调用参数
});

// 测试数据库是否连接成功
seq.authenticate().then(() => {
  console.log('数据库连接成功')
}).catch((err) => {
  console.log(err)
})

module.exports = seq;