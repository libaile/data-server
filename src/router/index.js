const fs = require('fs');

const Router = require('koa-router');
const router = new Router();

// 遍历当前目录下的所有文件 file => 文件名  __dirname 当前相对路径
fs.readdirSync(__dirname).forEach(file => {
  if (file !== 'index.js') {
    let r = require('./' + file);
    router.use(r.routes())
  }
})

module.exports = router;