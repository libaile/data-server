const Router = require('koa-router');

// prefix属性 用于指定路由的前缀
const router = new Router({ prefix: '/goods' });
// 比如 /upload 映射为 /goods/upload

const { auth, hadAdminPermission } = require('../middleware/auth.middleware');
const { upload } = require('../controller/goods.controller');

router.post('/upload', auth, hadAdminPermission, upload);

module.exports = router;
