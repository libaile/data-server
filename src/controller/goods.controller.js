const path = require('path');
const { errorRequest } = require('../utils/util');
const { uploadFileError } = require('../constant/err.type');

class goodsController {
  // path.basename() 解析文件路径里的文件名   C:\Users\Desktop\project\src\upload\xxx.png => xxx.png
  async upload(ctx, next) {
    const { file } = ctx.request.files;
    console.log(file.path)
    if (file) {
      ctx.body = {
        code: 0,
        message: '文件上传成功',
        result: {
          imgUrl: path.basename(file.path)
        }
      }
    } else {
      errorRequest(ctx, uploadFileError, '文件上传失败')
    }

    await next();
  }
}

module.exports = new goodsController();;