module.exports = {
  userFormatteError: {
    code: '10001',
    message: '用户名或密码为空',
    result: ''
  },
  userAlreadyExisted: {
    code: '10002',
    message: '用户名已经存在',
    result: ''
  },
  rejectRegisterError: {
    code: '10003',
    message: '用户注册失败',
    result: ''
  },
  userDoesNotExist: {
    code: '10004',
    message: '用户不存在',
    result: '',
  },
  userLoginError: {
    code: '10005',
    message: '用户登录失败',
    result: '',
  },
  invalidPassword: {
    code: '10006',
    message: '密码不匹配',
    result: '',
  },
  updatePasswordError: {
    code: '10007',
    message: '修改密码失败',
    result: '',
  },
  tokenExpiredError: {
    code: '10101',
    message: 'token已过期',
    result: '',
  },
  invalidToken: {
    code: '10102',
    message: '无效的token',
    result: '',
  },
  hasNotAdminPermission: {
    code: '10103',
    message: '没有管理员权限',
    result: '',
  },
  uploadFileError: {
    code: '10201',
    message: '文件上传失败',
    result: '',
  }
}