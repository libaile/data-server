// path 模块用于处理文件路径相关的操作
const path = require('path');

const Koa = require('koa')
// koa-body 应用场景包括
// 1.处理 post 请求的表单数据
// 2.处理 post 请求的JSON数据
// 3.处理上传的文件数据
// 4.处理PUT、PATCH 等请求的数据
const KoaBody = require('koa-body');

// 处理静态资源
const KoaStatic = require('koa-static');

const router = require('../router');
// 处理错误请求
const errHandler = require('./errHandle');

const app = new Koa()

// 文件上传逻辑
app.use(
  KoaBody({
    multipart: true,
    formidable: {
      // 1. 在配置选项 options，不推荐使用相对路径
      // 2. 在options 里的相对路径，不是相对的当前文件，而是相对于 process.cwd() 也就是脚本执行的地址
      // 3. path.join 将多个路径拼接成一个路径，并返回拼接后的路径
      uploadDir: path.join(__dirname, '../upload'),
      keepExtensions: true,
    }
  })
)

// 配置静态资源访问路径
app.use(KoaStatic(path.join(__dirname, '../upload')));

// 路由挂载
// router.routes() 方法返回一个中间件函数，该函数可以将路由对象挂载到Koa应用上
// allowedMethods 用于在路由无法处理某个http请求时，自动返回405 method not allowed 或 501 not implemented
app.use(router.routes()).use(router.allowedMethods());

// 统一的错误处理
app.on('error', errHandler)
module.exports = app;