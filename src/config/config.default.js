// 用于从环境变量中加载配置参数到Node.js应用程序中
const dotenv = require('dotenv');
// 加载环境变量
dotenv.config();

// 导出环境变量中的配置参数 也就是项目根路径下 .env 文件定义的配置参数
module.exports = process.env;